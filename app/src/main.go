package src

import (
	"fmt"
	"log"
	"net"

	"github.com/urfave/cli"
)

func Gerar() *cli.App {
	app := cli.NewApp()
	app.Name = "Buscar domínios"
	app.Usage = "Sistema de buscar de informações de domínio"
	app.Version = "0.1"

	flags := []cli.Flag{
		cli.StringFlag{
			Name:  "host",
			Value: "www.google.com.br",
		},
	}

	app.Commands = []cli.Command{
		{
			Name:   "ip",
			Usage:  "Busca endereço de ip's do host",
			Flags:  flags,
			Action: buscaIps,
		},
		{
			Name:   "servidor",
			Usage:  "Busca endereço dos servidore's do host",
			Flags:  flags,
			Action: buscaServidores,
		},
	}

	return app
}

func buscaIps(c *cli.Context) {
	host := c.String("host")

	ips, err := net.LookupIP(host)
	if err != nil {
		log.Panic(err)
	}

	filtroIp4 := len(ips) / 2
	for i, ip := range ips {
		if i >= filtroIp4 {
			fmt.Println(ip)
		}
	}
}

func buscaServidores(c *cli.Context) {
	host := c.String("host")

	servidores, err := net.LookupNS(host)
	if err != nil {
		log.Panic(err)
	}

	for _, servidor := range servidores {
		fmt.Println(servidor.Host)
	}
}
