package main

import (
	"fmt"
	"log"
	"os"
	"terminal_app/src"
)

func main() {
	fmt.Printf("Aplicação iniciada...\n")

	application := src.Gerar()
	if err := application.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
